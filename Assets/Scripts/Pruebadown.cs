﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pruebadown : MonoBehaviour
{

    private Transform playerTransform;
    private Vector3 newPos;

    public Transform ballPosition;

    public float speed;
    private float move;

    void Start()
    {
        // Equivalente a playerTransform = GetComponent<Transform> ();
        playerTransform = transform;

        newPos = playerTransform.position;
    }

    void Update()
    {
        if (ballPosition.position.x > transform.position.x)
        {
            if (ballPosition.position.y < 3)
            {
                move = 1.3f;
            }
            else
            {
                move = 0;
            }
        }
        else
        {
            if (ballPosition.position.y < 3)
            {
                move = -1.3f;
            }
            else
            {
                move = 0;
            }
        }
        move *= Time.deltaTime;
        move *= speed;
        transform.Translate(move, 0, 0);

        if (transform.position.x < -9.11f)
        {
            transform.position = new Vector3(-9.11f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > 9.11f)
        {
            transform.position = new Vector3(9.11f, transform.position.y, transform.position.z);
        }
    }
}